import Router from '@koa/router'
import promosRoutes from './promos/promos.routes'

const routes: Router[] = [promosRoutes]

export default routes
