import Router from '@koa/router'
import promosCtrl from './promos.ctrl'

const promosRouter = new Router({ prefix: '/promo' })

promosRouter.post('/', promosCtrl.createPromo)

promosRouter.get('/', promosCtrl.listPromosEntities)

promosRouter.get('/:id', promosCtrl.promoById)

promosRouter.put('/:id', promosCtrl.updatePromo)

promosRouter.delete('/:id', promosCtrl.deletePromo)

promosRouter.post('/:promoId/participant', promosCtrl.createParticipant)

promosRouter.delete('/:promoId/participant/:participantId', promosCtrl.deleteParticipant)

promosRouter.post('/:promoId/prize', promosCtrl.createPrize)

promosRouter.delete('/:promoId/prize/:prizeId', promosCtrl.deletePrize)

promosRouter.post('/:id/raffle', promosCtrl.rafflePromos)

export default promosRouter
