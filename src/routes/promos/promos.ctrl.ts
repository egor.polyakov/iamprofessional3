import { Context } from 'koa'
import { maybe, string, validate } from 'koa-sbv'
import promosService from './promos.service'
import { ICreatePromoDTO, IUpdatePromoDTO } from './promos.types'
import promosDb from './promos.db'

const createPromo = async (ctx: Context) => {
  // Вадидация body запроса
  const data: ICreatePromoDTO = validate(ctx.request.body, {
    name: string({ min: 1 }),
    description: maybe('string', ''),
  })

  ctx.body = await promosService.createPromo(data)
  ctx.status = 200
}

const listPromosEntities = async (ctx: Context) => {
  ctx.body = await promosService.listPromosEntities()
  ctx.status = 200
}

const promoById = async (ctx: Context) => {
  const id = +ctx.params.id

  // Проверка существования промо с указанным id
  const isPromoExists = await promosDb.isPromoExists(id)
  ctx.assert(isPromoExists, 401, 'Promo with given id does not exists')

  ctx.body = await promosService.promoById(id)
  ctx.status = 200
}

const updatePromo = async (ctx: Context) => {
  const id = +ctx.params.id

  // Проверка существования промо с указанным id
  const isPromoExists = await promosDb.isPromoExists(id)
  ctx.assert(isPromoExists, 401, 'Promo with given id does not exists')

  // Вадидация body запроса
  const updates: IUpdatePromoDTO = validate(ctx.request.body, {
    name: string({ min: 1 }),
    description: 'string',
  })

  await promosService.updatePromo(updates)
  ctx.body = ''
  ctx.status = 200
}

const deletePromo = async (ctx: Context) => {
  const id = +ctx.params.id

  // Проверка существования промо с указанным id
  const isPromoExists = await promosDb.isPromoExists(id)
  ctx.assert(isPromoExists, 401, 'Promo with given id does not exists')

  await promosService.deletePromo(id)

  ctx.body = ''
  ctx.status = 200
}

const createPrize = async (ctx: Context) => {
  const id = +ctx.params.promoId

  // Проверка существования промо с указанным id
  const isPromoExists = await promosDb.isPromoExists(id)
  ctx.assert(isPromoExists, 401, 'Promo with given id does not exists')

  const body = validate(ctx.request.body, { description: string({ min: 1 }) })

  ctx.body = await promosService.createPrize(id, body)
  ctx.status = 200
}

const deletePrize = async (ctx: Context) => {
  const promoId = +ctx.params.promoId

  // Проверка существования промо с указанным id
  const isPromoExists = await promosDb.isPromoExists(promoId)
  ctx.assert(isPromoExists, 401, 'Promo with given id does not exists')

  const prizeId = +ctx.params.prizeId

  ctx.body = await promosService.deletePrize(prizeId, prizeId)
  ctx.status = 200
}

const createParticipant = async (ctx: Context) => {
  const id = +ctx.params.promoId

  // Проверка существования промо с указанным id
  const isPromoExists = await promosDb.isPromoExists(id)
  ctx.assert(isPromoExists, 401, 'Promo with given id does not exists')

  const body = validate(ctx.request.body, { name: string({ min: 1 }) })

  ctx.body = await promosService.createParticipant(id, body)
  ctx.status = 200
}

const deleteParticipant = async (ctx: Context) => {
  const id = +ctx.params.promoId

  // Проверка существования промо с указанным id
  const isPromoExists = await promosDb.isPromoExists(id)
  ctx.assert(isPromoExists, 401, 'Promo with given id does not exists')

  const participantId = +ctx.params.participantId

  ctx.body = await promosService.deleteParticipant(id, participantId)
  ctx.status = 200
}

const rafflePromos = async (ctx: Context) => {
  const promoId = +ctx.params.id

  // Проверка существования промо с указанным id
  const isPromoExists = await promosDb.isPromoExists(promoId)
  ctx.assert(isPromoExists, 401, 'Promo with given id does not exists')

  const isRafflePossible = await promosService.checkIsRafflePossible(promoId)
  ctx.assert(isRafflePossible, 409, 'Raffle is not possible')

  ctx.body = await promosService.makeRaffle(promoId)
  ctx.status = 200
}

const promosCtrl = {
  createPromo,
  listPromosEntities,
  updatePromo,
  deletePromo,
  rafflePromos,
  promoById,
  createPrize,
  deletePrize,
  createParticipant,
  deleteParticipant,
}
export default promosCtrl
