import { IParticipantEntity, IPrizeEntity, IRaffleResult } from './promos.types'
import assert from '../../shared/assert'

const makeRaffle = (participants: IParticipantEntity[], prizes: IPrizeEntity[]): IRaffleResult[] => {
  if (participants.length !== prizes.length) {
    assert(false, 409, 'Participants and prizes count are not equal')
  }

  const results: IRaffleResult[] = []

  // Каждому призу с индексом i сопоставляем приз с индексом n - i - 1
  for (let i = 0; i < participants.length; ++i) {
    results.push({ winner: participants[participants.length - i - 1], prize: prizes[i] })
  }

  return results
}

const raffleService = {
  makeRaffle,
}

export default raffleService
