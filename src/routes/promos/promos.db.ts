import {
  ICreateParticipantDTO,
  ICreatePrizeDTO,
  ICreatePromoDTO,
  IParticipantEntity,
  IPrizeEntity,
  IPromoEntity,
  IUpdatePromoDTO,
} from './promos.types'
import db, { DB } from '../../db'

const prizesCount = async (id: IPromoEntity['id'], connection: DB = db): Promise<number> => {
  const count = await connection.query(`SELECT count(*) FROM promo_prizes WHERE promo_id = $1`, [id])

  return count.rows[0].count
}

const participantsCount = async (id: IPromoEntity['id'], connection: DB = db): Promise<number> => {
  const count = await connection.query(`SELECT count(*) FROM promo_participants WHERE promo_id = $1`, [id])

  return count.rows[0].count
}

const isPromoExists = async (id: IPromoEntity['id'], connection: DB = db): Promise<boolean> => {
  const promo = await connection.selectOne(`SELECT id FROM promos WHERE id = $1`, [id])

  return !!promo.id
}

const createPromo = async (data: ICreatePromoDTO, connection: DB = db): Promise<IPromoEntity> => {
  const dbResp = await connection.insertOne(`INSERT INTO promos ? RETURNING *`, data)

  return dbResp
}

const promosEntityList = async (connection: DB = db): Promise<IPromoEntity[]> => {
  const dbResp = await connection.select(`SELECT * FROM promos`)

  return dbResp
}

const promoEntityById = async (id: IPromoEntity['id'], connection: DB = db): Promise<IPromoEntity> => {
  const dbResp = await connection.selectOne(`SELECT * FROM promos WHERE id=$1`, [id])

  return dbResp
}

const updatePromo = async (updates: IUpdatePromoDTO, connection: DB = db): Promise<IPromoEntity> => {
  const dbResp = await connection.updateOne(`UPDATE promos SET ? RETURNING *`, updates)

  return dbResp
}

const deletePromo = async (id: IPromoEntity['id'], connection: DB = db): Promise<IPromoEntity> => {
  const dbResp = await connection.selectOne(`DELETE FROM promos WHERE id = $1 RETURNING *`, [id])

  return dbResp
}

const listParticipants = async (id: IPromoEntity['id'], connection: DB = db): Promise<IParticipantEntity[]> => {
  const promoParticipants = await connection.select(
    `SELECT participant_id FROM promo_participants WHERE promo_id = $1`,
    [id]
  )

  const ids = promoParticipants.map((p) => p.participant_id)
  if (!ids.length) return []

  const dbResp = await connection.select(`SELECT id, name FROM participants WHERE id IN (${ids.join(', ')})`)

  return dbResp
}

const listPrizes = async (id: IPromoEntity['id'], connection: DB = db): Promise<IPrizeEntity[]> => {
  const promoPrizes = await connection.select(`SELECT prize_id FROM promo_prizes WHERE promo_id = $1`, [id])

  const ids = promoPrizes.map((p) => p.prize_id)
  if (!ids.length) return []

  const dbResp = await connection.select(`SELECT * FROM prizes WHERE id IN (${ids.join(', ')})`)

  return dbResp
}

const createPrize = async (
  promo_id: IPromoEntity['id'],
  prize: ICreatePrizeDTO,
  connection: DB = db
): Promise<IPrizeEntity> => {
  const created = await connection.insertOne(`INSERT INTO prizes ? RETURNING *`, prize)
  await connection.insertOne(`INSERT INTO promo_prizes ?`, { promo_id, prize_id: created.id })

  return created
}

const deletePrize = async (
  promo_id: IPromoEntity['id'],
  prize_id: IPrizeEntity['id'],
  connection: DB = db
): Promise<void> => {
  const removed = await connection.selectOne(`DELETE FROM prizes WHERE id = $1 RETURNING *`, [prize_id])
  await connection.query(`DELETE FROM promo_prizes WHERE promo_id = $1 AND prize_id = $2`, [promo_id, prize_id])

  return removed
}

const createParticipant = async (
  promo_id: IPromoEntity['id'],
  participant: ICreateParticipantDTO,
  connection: DB = db
): Promise<IParticipantEntity> => {
  const created = await connection.insertOne(`INSERT INTO participants ? RETURNING *`, participant)
  await connection.insertOne(`INSERT INTO promo_participants ?`, { promo_id, participant_id: created.id })

  return created
}

const deleteParticipant = async (
  promo_id: IPromoEntity['id'],
  participant_id: IPrizeEntity['id'],
  connection: DB = db
): Promise<IParticipantEntity> => {
  const removed = await connection.selectOne(`DELETE FROM participants WHERE id = $1 RETURNING *`, [participant_id])
  await connection.query(`DELETE FROM promo_participants WHERE promo_id = $1 AND participant_id = $2`, [
    promo_id,
    participant_id,
  ])

  return removed
}

const promosDb = {
  isPromoExists,
  createPromo,
  promosEntityList,
  updatePromo,
  deletePromo,
  prizesCount,
  participantsCount,
  listParticipants,
  listPrizes,
  promoEntityById,
  createPrize,
  deletePrize,
  createParticipant,
  deleteParticipant,
}
export default promosDb
