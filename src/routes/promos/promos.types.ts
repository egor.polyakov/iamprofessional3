export interface ICreatePromoDTO {
  name: string
  description: string
}

export interface IPromoEntity extends ICreatePromoDTO {
  id: number
}

export type IUpdatePromoDTO = ICreatePromoDTO

export interface IRaffleResult {
  winner: IParticipantEntity
  prize: IPrizeEntity
}

export interface IParticipantEntity {
  id: number
  name: string
}

export interface IPrizeEntity {
  id: number
  description: string
}

export interface IPromo extends IPromoEntity {
  prizes: IPrizeEntity[]
  participants: IParticipantEntity[]
}

export interface ICreatePrizeDTO {
  description: string
}

export interface ICreateParticipantDTO {
  name: string
}
