import {
  ICreatePrizeDTO,
  ICreatePromoDTO,
  ICreateParticipantDTO,
  IPrizeEntity,
  IPromo,
  IPromoEntity,
  IRaffleResult,
  IUpdatePromoDTO,
  IParticipantEntity,
} from './promos.types'
import promosDb from './promos.db'
import raffleService from './raffle.service'

const checkIsRafflePossible = async (id: IPromoEntity['id']): Promise<boolean> => {
  const [prizesCount, participantsCount] = await Promise.all([promosDb.prizesCount(id), promosDb.participantsCount(id)])

  return prizesCount === participantsCount
}

const createPromo = async (data: ICreatePromoDTO): Promise<IPromoEntity['id']> => {
  const created = await promosDb.createPromo(data)

  return created.id
}

const listPromosEntities = async (): Promise<IPromoEntity[]> => {
  const list = await promosDb.promosEntityList()

  return list
}

const promoById = async (id: IPromoEntity['id']): Promise<IPromo> => {
  const promoEntity = await promosDb.promoEntityById(id)
  const promoPrizes = await promosDb.listPrizes(id)
  const promoParticipants = await promosDb.listParticipants(id)

  return { ...promoEntity, prizes: promoPrizes, participants: promoParticipants }
}

const updatePromo = async (updates: IUpdatePromoDTO): Promise<void> => {
  await promosDb.updatePromo(updates)
}

const deletePromo = async (id: IPromoEntity['id']): Promise<void> => {
  await promosDb.deletePromo(id)
}

const makeRaffle = async (id: IPromoEntity['id']): Promise<IRaffleResult[]> => {
  const prizes = await promosDb.listPrizes(id)
  const participants = await promosDb.listParticipants(id)

  return raffleService.makeRaffle(participants, prizes)
}

const createPrize = async (promo_id: IPromoEntity['id'], prize: ICreatePrizeDTO): Promise<IPrizeEntity['id']> => {
  const created = await promosDb.createPrize(promo_id, prize)

  return created.id
}

const deletePrize = async (promo_id: IPromoEntity['id'], prize_id: IPrizeEntity['id']): Promise<void> => {
  await promosDb.deletePrize(promo_id, prize_id)
}

const createParticipant = async (
  promo_id: IPromoEntity['id'],
  participant: ICreateParticipantDTO
): Promise<IPrizeEntity['id']> => {
  const created = await promosDb.createParticipant(promo_id, participant)

  return created.id
}

const deleteParticipant = async (
  promo_id: IPromoEntity['id'],
  participant_id: IParticipantEntity['id']
): Promise<void> => {
  await promosDb.deleteParticipant(promo_id, participant_id)
}

const promosService = {
  createPromo,
  listPromosEntities,
  updatePromo,
  deletePromo,
  checkIsRafflePossible,
  makeRaffle,
  promoById,
  createPrize,
  deletePrize,
  createParticipant,
  deleteParticipant,
}
export default promosService
