const { fork, spawn } = require('child_process')
const path = require('path')

/* console utils, color output in red or green */
const red = (text) => console.log('\u001b[31m' + text + '\u001b[0m')
const green = (text) => console.log('\u001b[32m' + text + '\u001b[0m')

/* project root directory and path to the main file to start the application */
const root = path.resolve('.')
const entrypoint = path.resolve(root, 'dist', 'app.js')

/* forked application process, instance of ChildProcess */
let server = null

/* launch TypeScript compiler in watch mode and restart the app after each rebuild */
const tsc = spawn('npm run tsc:watch', { cwd: root, shell: true })

tsc.on('error', (e) => console.error('Can not start TypeScript compiler', e))

tsc.stdout.on('data', (data) => {
  console.log(data.toString())
  restartServer()
})

tsc.stderr.on('data', (data) => {
  console.error(data.toString())
  restartServer()
})

/* kill previous instance if it's still running and start new one */
const restartServer = debounce(() => {
  if (server && !server.killed) {
    server.kill()
    server.once('exit', startServer)
  } else {
    startServer()
  }
}, 1000)

/* start app.js in child process and pipe its stdout into current process stdout */
const startServer = () => {
  green(`starting ${entrypoint}`)
  server = fork(entrypoint, [], { stdio: 'inherit' })

  server.once('exit', () => {
    red('server exited')
    server = null
  })
}

/* helper function to prevent restarting server too often */
function debounce(fn, time) {
  let timeout = null

  return (...args) => {
    clearTimeout(timeout)
    timeout = setTimeout(() => fn(...args), time)
  }
}
