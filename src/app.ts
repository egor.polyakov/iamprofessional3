import Koa from 'koa'
import parser from 'koa-body'
import config from './config'
import './routes'
import * as sbv from 'koa-sbv'
import logger from './shared/logger'
import * as migrate from './db/migration'
import { cors, errorHandler, requestLogger } from './app/middlware'
import { superuserPool, workerPool } from './db'
import routes from './routes'
import serve from 'koa-static'
import mount from 'koa-mount'
import path from 'path'

const app = new Koa()
app.proxy = true

if (config.cors) app.use(cors)

app
  .use(errorHandler)
  .use(parser({ urlencoded: false, jsonLimit: '50mb', multipart: true }))
  .use(requestLogger)
  .use(sbv.middleware)
  .use(mount('/apidoc', serve(path.resolve('apidoc'))))

const listen = () => {
  const server = app.listen(config.port, () =>
    logger.info(`process ${process.pid} listening on port ${config.port}`, 'app')
  )

  // graceful shutdown
  process.on('SIGTERM', () => {
    logger.info('Received SIGTERM signal, starting shutdown...', 'app')

    const close = async () => {
      logger.info('Closed http server, closing db connections...', 'app')
      await Promise.all([superuserPool.end(), workerPool.end()])

      logger.info('Closed db connections, terminating process', 'app')
      process.exit(0)
    }

    server.close(close)
    setTimeout(close, 3000).unref()
  })
}

const abort = (err) => logger.fatal(err, 'migration')

const start = async () => {
  await migrate.up()

  for (const router of routes) {
    app.use(router.routes()).use(router.allowedMethods())
  }

  listen()
}

start().catch(abort)
