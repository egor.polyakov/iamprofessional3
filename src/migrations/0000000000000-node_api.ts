import { Connection } from 'pg'
import config from '../config'

/**
 * Создание нового пользователя с правами ограниченными базовыми CRUD операциями для того чтобы
 * использовать его по умолчанию и минимизировать кол-во запросов отправляемых с правами суперпользователя.
 */
module.exports.up = async (db: Connection) => {
  await db.query(`REVOKE ALL ON schema public FROM public`)
  await db.query(`CREATE USER node_api WITH LOGIN PASSWORD '${config.postgresPassword}'`)
  await db.query(`GRANT CONNECT ON DATABASE ${config.postgresDatabase} TO node_api`)
  await db.query(`GRANT USAGE ON SCHEMA public TO node_api`)
  await db.query(`GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO node_api`)
  await db.query(`GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO node_api`)

  await db.query(`ALTER USER ${config.postgresUser} SET idle_in_transaction_session_timeout TO 60000`)
  await db.query(`ALTER USER node_api SET idle_in_transaction_session_timeout TO 60000`)
}

module.exports.down = async (db: Connection) => {
  await db.query(`DROP OWNED BY node_api`)
  await db.query(`DROP USER node_api`)
}
