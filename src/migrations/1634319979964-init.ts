import Connection from '../db/db.connection'

module.exports.up = async (db: Connection) => {
  await db.query(`
    CREATE TABLE IF NOT EXISTS promos (
      id serial PRIMARY KEY,
      name text NOT NULL,
      description text NOT NULL
    )`)

  await db.query(`
    CREATE TABLE IF NOT EXISTS prizes (
     id serial PRIMARY KEY,
     description text NOT NULL
  )`)

  await db.query(`
     CREATE TABLE IF NOT EXISTS participants (
      id serial PRIMARY KEY,
      name text NOT NULL
    )`)

  await db.query(`
     CREATE TABLE IF NOT EXISTS promo_results (
      winner_id integer NOT NULL,
      prize_id integer NOT NULL,

      FOREIGN KEY (winner_id) REFERENCES participants (id) ON DELETE CASCADE,
      FOREIGN KEY (prize_id) REFERENCES prizes (id) ON DELETE CASCADE
  )`)

  await db.query(`
    CREATE TABLE IF NOT EXISTS promo_prizes (
     promo_id integer NOT NULL,
     prize_id integer NOT NULL,

     FOREIGN KEY (promo_id) REFERENCES promos (id) ON DELETE CASCADE,
     FOREIGN KEY (prize_id) REFERENCES prizes (id) ON DELETE CASCADE
  )`)

  await db.query(`
    CREATE TABLE IF NOT EXISTS promo_participants (
    promo_id integer NOT NULL,
    participant_id integer NOT NULL,

    FOREIGN KEY (promo_id) REFERENCES promos (id) ON DELETE CASCADE,
    FOREIGN KEY (participant_id) REFERENCES participants (id) ON DELETE CASCADE
  )`)

  await db.query(`GRANT ALL PRIVILEGES ON TABLE promo_participants TO node_api`)
  await db.query(`GRANT ALL PRIVILEGES ON TABLE promo_prizes TO node_api`)
  await db.query(`GRANT ALL PRIVILEGES ON TABLE promo_results TO node_api`)
  await db.query(`GRANT ALL PRIVILEGES ON TABLE participants TO node_api`)
  await db.query(`GRANT ALL PRIVILEGES ON TABLE prizes TO node_api`)
  await db.query(`GRANT ALL PRIVILEGES ON TABLE promos TO node_api`)
  await db.query(`GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO node_api`)
}

module.exports.down = async (db: Connection) => {
  await db.query(`DROP TABLE promo_participants`)
  await db.query(`DROP TABLE promo_prizes`)
  await db.query(`DROP TABLE promo_results`)
  await db.query(`DROP TABLE participants`)
  await db.query(`DROP TABLE prizes`)
  await db.query(`DROP TABLE promos`)
}
