import logger from './shared/logger'

const mode = process.env.NODE_ENV
const production = mode === 'production'

function env(name: string, defaultValue?: string): string
function env(name: string, defaultValue?: number): number

function env(name: string, defaultValue?: any): any {
  const value = process.env[name]

  if (value == null && defaultValue == null && mode !== 'test') {
    logger.error(`${process.env.NODE_ENV} Required environment variable ${name} is not defined`, 'config')
    process.exit(1)
  }

  if (value == null) {
    logger.debug(`${name} is not defined, using default value ${defaultValue}`, 'config')
    return defaultValue
  }

  if (typeof defaultValue === 'number') {
    const isnan = Number.isNaN(+value)

    if (isnan && defaultValue == null) {
      throw new Error(`Can not parse env variable ${name} as number`)
    }

    return isnan ? defaultValue : +value
  } else {
    return value
  }
}

const config = {
  production,
  cors: env('CORS', ''),
  jwtKey: env('JWT_KEY', '..............topsecret............'),
  port: env('PORT', 3000),
  redisUrl: env('REDIS_URL', 'au-nsi-redis:6379'),
  postgresDatabase: env('POSTGRES_DATABASE', 'template1'),
  postgresHost: env('POSTGRES_HOST', 'au-nsi-postgres'),
  postgresPassword: env('POSTGRES_PASSWORD'),
  postgresPort: env('POSTGRES_PORT', 5432),
  postgresUser: env('POSTGRES_USER'),
  minioAccessKey: env('MINIO_ACCESS_KEY', ''),
  minioSecretKey: env('MINIO_SECRET_KEY', ''),
  minioUrl: env('MINIO_URL', 'http://au-minio:9000'),
}

export default config
