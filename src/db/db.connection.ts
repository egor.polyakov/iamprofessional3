import { PoolClient } from 'pg'
import DBClient from './db.client'

export default class DBConnection extends DBClient<PoolClient> {
  constructor(client: PoolClient) {
    super(client)
  }

  public release() {
    return this.client.release()
  }
}
