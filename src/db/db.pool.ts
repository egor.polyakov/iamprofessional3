import { Pool } from 'pg'
import DBClient from './db.client'
import DBConnection from './db.connection'

export default class DBPool extends DBClient<Pool> {
  constructor(pool: Pool) {
    super(pool)
  }

  public async transaction(fn: (connection: DBConnection) => Promise<any>) {
    const connection = await this.getConnection()
    let result

    try {
      await connection.query('BEGIN')
      result = await fn(connection)
      await connection.query('COMMIT')
    } catch (error) {
      await connection.query('ROLLBACK')
      throw error
    } finally {
      connection.release()
    }

    return result
  }

  public async getConnection() {
    const connection = await this.client.connect()
    return new DBConnection(connection)
  }

  public close() {
    return this.client.end()
  }
}
