import { Pool, PoolConfig } from 'pg'
import config from '../config'
import DBPool from './db.pool'
import DBConnection from './db.connection'

const pgConfig: PoolConfig = {
  user: config.postgresUser,
  host: config.postgresHost as string,
  port: config.postgresPort,
  database: config.postgresDatabase,
  password: config.postgresPassword,
  idleTimeoutMillis: 60_000,
  connectionTimeoutMillis: 10_000,
}

export type DB = DBPool | DBConnection

export const superuserPool = new Pool(pgConfig)
export const workerPool = new Pool({ ...pgConfig, user: 'node_api' }) // пользователь создается в миграции

// соединение с правами суперпользователя, должно использоваться только для применения миграций
export const superuser = new DBPool(superuserPool)

// соединение с базовыми CRUD правами, используется для обработки всех API запросов
const db = new DBPool(workerPool)
export default db
