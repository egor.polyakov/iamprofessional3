import fs from 'fs'
import path from 'path'
import logger from '../shared/logger'
import { superuser } from './index'

/**
 * Apply all new migrations up to and including *name*
 * If *name* is not specified all new migrations will be applied
 */
export const up = async (name?: string) => {
  return migrate(async (lastRun, migrations, connection) => {
    // remove migrations that have already been run
    let list = migrations.filter((m) => m.name > lastRun)

    // if *name* is specified remove all cosequent migrations
    if (name) {
      list = list.filter((m) => m.name <= name)
    }

    for (const item of list) {
      logger.info(`Running migration ${item.name}`, 'migration')
      await item.migration.up(connection)
    }

    return list.length > 0 ? list[list.length - 1].name : lastRun
  })
}

/**
 * Roll back all applied migrations up to and including *name*
 * If *name* is not specified all migrations will be reverted
 */
export const down = async (name?: string) => {
  return migrate(async (lastRun, migrations, connection) => {
    // remove migrations that have not yet been run
    let list = migrations.filter((m) => m.name <= lastRun)

    if (name) {
      list = list.filter((m) => m.name >= name)
    }

    // roll back migrations in reverse order
    list.reverse()

    for (const item of list) {
      logger.info(`Rolling back migration ${item.name}`, 'migration')
      await item.migration.down(connection)
    }

    // find last migration that remained applied
    const remaining = name ? migrations.filter((m) => m.name < name) : []
    return remaining.length > 0 ? remaining[remaining.length - 1].name : ''
  })
}

/**
 * require all files in the *migrations* directory sorted by name
 */
const loadMigrations = () => {
  const migrationsDir = path.join(process.cwd(), 'dist', 'migrations')
  const files = fs.readdirSync(migrationsDir).sort()

  return files.map((name) => {
    const migration = require(path.join(migrationsDir, name))
    return { name, migration }
  })
}

/**
 * Some helper code that is required to apply or roll back migrations
 */
const migrate = async (cb) => {
  const migrations = loadMigrations()

  await superuser.transaction(async (connection) => {
    await connection.query(`
      CREATE TABLE IF NOT EXISTS migrations (
        id integer PRIMARY KEY,
        data jsonb NOT NULL
      )
    `)

    // load information about already applied migrations
    await connection.query('LOCK TABLE migrations IN ACCESS EXCLUSIVE MODE')
    const info = await connection.selectOne('SELECT data FROM migrations')
    const lastRun = info ? info.data.lastRun : ''

    // perform migration
    const result = await cb(lastRun, migrations, connection)

    // save information about migration result
    await connection.query(
      `
      INSERT INTO migrations (id, data) VALUES (1, $1)
      ON CONFLICT (id) DO UPDATE SET data = $1
    `,
      [{ lastRun: result }]
    )
  })
}
