import { Pool, PoolClient, types } from 'pg'
import assert from '../shared/assert'
import logger from '../shared/logger'

types.setTypeParser(20, 'text', parseInt)

export default class DBClient<T extends Pool | PoolClient> {
  public errors = {
    foreign_key_violation: '23503',
    unique_violation: '23505',
  }

  constructor(protected client: T) {}

  public async query(sql: string, params?: any[], errors: Record<string, string> = {}) {
    try {
      const result = await this.client.query(sql, params)

      logger.debug(`DB QUERY: %s\nARGS: %j\nRESP: %j`, 'db', sql, params, result.rows)

      return result
    } catch (error: any) {
      // human-readable messages for expected errors (such as unique violation, etc.)
      if (errors[error.code]) {
        assert(false, 409, errors[error.code])
      }

      throw error
    }
  }

  public select(sql: string, params?: any[]) {
    return this.query(sql, params).then((result) => result.rows)
  }

  public selectOne(sql: string, params?: any[], errors?: Record<string, string>) {
    return this.query(sql, params, errors).then((result) => result.rows[0])
  }

  public selectAsArray(sql: string, params?: any[]) {
    return this.client.query({ text: sql, values: params, rowMode: 'array' })
  }

  public async insertOne(sql: string, data: any, errors?: Record<string, string>) {
    const query = this.formatInsertQuery(sql, [data])
    const result = await this.query(query.text, query.values, errors)
    return result.rows[0]
  }

  public async insertMany(sql: string, data: any[]) {
    const query = this.formatInsertQuery(sql, data)
    const result = await this.query(query.text, query.values)
    return result.rows
  }

  public async updateOne(sql: string, data: any, params: any[] = [], errors?: Record<string, string>) {
    const query = this.formatUpdateQuery(sql, data, params)
    const result = await this.query(query.text, query.values, errors)
    return result.rows[0]
  }

  /**
   * Синтаксический сахар для inserts
   * from: 'INSERT INTO user ?'
   * to: 'INSERT INTO user (username, email) VALUES ($1, $2), ($3, $4)'
   */
  private formatInsertQuery(sql: string, data: any[]) {
    // находим названия всех свойств из всех объектов массива
    const keys = new Set<string>()

    for (const item of data) {
      for (const key of Object.keys(item)) {
        keys.add(key)
      }
    }

    const names = [...keys].join(', ')

    const indexes: string[] = [] // индексы параметров $1, $2 и т.д.
    const params: any[] = [] // значения самих параметров
    let index = 1

    // создаем параметризированный SQL запрос
    for (const item of data) {
      const row = []

      for (const key of keys) {
        row.push(`$${index}`)
        params.push(item[key] ?? null)
        index += 1
      }

      indexes.push(`(${row.join(', ')})`)
    }

    // заменяем '?' из исходного sql на полученное выражение
    const replacement = `(${names}) VALUES ${indexes.join(', ')}`
    const text = sql.replace('?', replacement)

    return { text, values: params }
  }

  /**
   * Синтаксический сахар для updates
   * from: 'UPDATE user SET ? WHERE id = 1'
   * to: 'UPDATE user SET username = $1, email = $2 WHERE id = 1'
   */
  private formatUpdateQuery(sql: string, data: any, params: any[]) {
    const set: string[] = []
    let index = params.length + 1

    for (const key of Object.keys(data)) {
      set.push(`${key} = $${index}`)
      params.push(data[key])
      index += 1
    }

    const text = sql.replace('?', set.join(', '))
    return { text, values: params }
  }
}
