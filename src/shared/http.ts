import http from 'http'
import https from 'https'
import { URL } from 'url'

class Http {
  private https: boolean
  private hostname: string
  private port: number
  private headers: Headers

  constructor(base: string, headers?: Headers) {
    const url = new URL(base.startsWith('http') ? base : 'http://' + base)
    this.https = url.protocol === 'https:'
    this.hostname = url.hostname
    this.port = url.port ? parseInt(url.port) : undefined
    this.headers = this.normalizeHeaders(headers)
  }

  get(path: string, options: IOptions = {}) {
    return this.request({ ...options, method: 'GET', path })
  }

  head(path: string, options: IOptions = {}) {
    return this.request({ ...options, method: 'HEAD', path })
  }

  post(path: string, body?: any, options: IOptions = {}) {
    return this.request({ ...options, method: 'POST', body, path })
  }

  put(path: string, body?: any, options: IOptions = {}) {
    return this.request({ ...options, method: 'PUT', body, path })
  }

  patch(path: string, body?: any, options: IOptions = {}) {
    return this.request({ ...options, method: 'PATCH', body, path })
  }

  delete(path: string, options: IOptions = {}) {
    return this.request({ ...options, method: 'DELETE', path })
  }

  async xget(path: string, options: IOptions = {}) {
    const r = await this.request({ ...options, method: 'GET', path })
    return r.body
  }

  async xpost(path: string, body?: any, options: IOptions = {}) {
    const r = await this.request({ ...options, method: 'POST', body, path })
    return r.body
  }

  async xput(path: string, body?: any, options: IOptions = {}) {
    const r = await this.request({ ...options, method: 'PUT', body, path })
    return r.body
  }

  async xpatch(path: string, body?: any, options: IOptions = {}) {
    const r = await this.request({ ...options, method: 'PATCH', body, path })
    return r.body
  }

  async xdelete(path: string, options: IOptions = {}) {
    const r = await this.request({ ...options, method: 'DELETE', path })
    return r.body
  }

  request(options: IRequestOptions): Promise<IResponse> {
    const headers = this.normalizeHeaders(options.headers, this.headers)
    const { body, type } = this.serializeBody(options.body)

    if (type && !headers['content-type']) {
      headers['content-type'] = type
    }

    const { method, path, timeout = 15_000, ca } = options
    const requestOptions = { hostname: this.hostname, port: this.port, path, method, headers, ca, timeout }

    return new Promise((resolve, reject) => {
      const agent = this.https ? https : http

      const req = agent.request(requestOptions, (res) => {
        const { statusCode, headers } = res

        const chunks = []
        res.on('data', (chunk) => chunks.push(chunk))

        res.on('end', () => {
          const buffer = Buffer.concat(chunks)
          const body = this.parseBody(buffer, headers['content-type'])

          if (statusCode >= 400 && options.throwHttpErrors !== false) {
            reject(this.createError(method, path, statusCode, buffer.toString()))
          }

          resolve({ body, headers, statusCode, options })
        })
      })

      req.on('error', (err) => {
        reject(this.createError(method, path, null, err.message))
      })

      req.on('timeout', () => {
        reject(this.createError(method, path, null, 'request timeout'))
        req.abort()
      })

      if (body) req.write(body)
      req.end()
    })
  }

  private serializeBody(body: any) {
    if (!body) return { body: null, type: null }
    else if (Buffer.isBuffer(body)) return { body, type: 'application/octet-stream' }
    else if (typeof body === 'string') return { body, type: 'text/plain' }
    else return { body: JSON.stringify(body), type: 'application/json' }
  }

  private parseBody(body: Buffer, type = '') {
    if (type.startsWith('application/json')) return jsonparse(body.toString('utf8'))

    const isBinary =
      type.startsWith('application/octet-stream') ||
      type.startsWith('image/') ||
      type.startsWith('audio/') ||
      type.startsWith('application/')

    return isBinary ? body : body.toString('utf8')
  }

  private normalizeHeaders(headers: Headers, defaults: Headers = {}) {
    const result = { ...defaults }

    if (headers) {
      for (const [key, value] of Object.entries(headers)) {
        result[key.toLowerCase()] = value
      }
    }

    return result
  }

  private createError(method: string, path: string, status: number, msg: string) {
    const err: any = new Error(`${method} ${path} failed with status ${status}: ${msg}`)
    err.statusCode = status
    return err
  }
}

const jsonparse = (str: string) => {
  try {
    return JSON.parse(str)
  } catch (error) {
    return str
  }
}

type Headers = { [key: string]: string }

interface IOptions {
  headers?: Headers
  throwHttpErrors?: boolean
  timeout?: number
  ca?: Buffer
}

export interface IRequestOptions extends IOptions {
  method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE' | 'HEAD'
  path?: string
  body?: any
}

export interface IResponse {
  body: any
  headers: { [key: string]: string | string[] }
  statusCode: number
  options: IRequestOptions
}

export default Http
