const assert = (condition: any, code: number, msg: string) => {
  if (!condition) {
    const err = new Error(msg) as any
    err.status = code
    err.expose = true
    throw err
  }
}

export default assert
