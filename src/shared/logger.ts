/* eslint no-console: "off" */
import { format } from 'util'

type LogLevel = 'DEBUG' | 'INFO' | 'WARN' | 'ERROR' | 'FATAL'

class Logger {
  private level = 2
  private sourceFilter = false
  private sourceWhitelist: Array<{ wildcard: boolean; value: string }> = []

  private levels = {
    DEBUG: 1,
    INFO: 2,
    WARN: 3,
    ERROR: 4,
    FATAL: 5,
  }

  constructor(level: LogLevel = 'INFO', sources: string) {
    this.level = this.levels[level]

    if (this.level == null) {
      throw new Error(`Invalid log level: ${level}`)
    }

    if (sources && sources !== '*') {
      this.sourceFilter = true

      this.sourceWhitelist = sources.split(',').map((source) => {
        const wildcard = source.endsWith('*')
        const value = wildcard ? source.slice(0, -1) : source
        return { wildcard, value }
      })
    }
  }

  public debug(msg: string, source: string, ...args) {
    return this.shouldLog('DEBUG', source) && this.log('DEBUG', source, msg, ...args)
  }

  public info(msg: string, source: string, ...args) {
    return this.shouldLog('INFO', source) && this.log('INFO', source, msg, ...args)
  }

  public warn(msg: string, source: string, ...args) {
    return this.shouldLog('WARN', source) && this.log('WARN', source, msg, ...args)
  }

  public error(err: Error | string, source: string, ...args) {
    return this.log('ERROR', source, this.err2str(err), ...args)
  }

  public fatal(err: Error | string, source: string, exitProcess = true) {
    this.log('FATAL', source, this.err2str(err))
    if (exitProcess) process.exit(1)
  }

  private shouldLog(level: LogLevel, source: string) {
    // уровень сообщения ниже выставленного уровня логирования
    if (this.levels[level] < this.level) return false

    // отсутствует фильтрация по источнику либо уровень сообщения выше предупреждения
    if (!this.sourceFilter || this.levels[level] > 2) return true

    // проверяем выставлено ли логирование для источника сообщения
    return this.sourceWhitelist.some((entry) => {
      return entry.wildcard ? source.startsWith(entry.value) : source === entry.value
    })
  }

  private log(level: LogLevel, source: string, msg: string, ...args) {
    let message = format(msg, ...args)

    if (PRODUCTION) {
      message = message.replace(/"/g, `\\"`).replace(/\n/g, '\\n')
    }

    console.log(`lvl=${level} src="${source}" msg="${message}"`)
  }

  private err2str(err: Error | string) {
    return err instanceof Error ? err.message + '\n' + err.stack : err
  }
}

const LEVEL: any = (process.env.LOG_LEVEL || 'INFO').toUpperCase()
const SOURCE = process.env.LOG_SOURCE
const PRODUCTION = process.env.NODE_ENV === 'production'

const logger = new Logger(LEVEL, SOURCE)

process.on('uncaughtException', (err) => {
  logger.fatal(err, 'global')
})

process.on('unhandledRejection', (reason: Error) => {
  logger.fatal(`Unhandled rejection at: ${reason.stack}, reason: ${reason}`, 'global')
})

export default logger
