import { Middleware } from 'koa'
import logger from '../shared/logger'

const ms = BigInt(1e6) // nanoseconds

export const requestLogger: Middleware = async (ctx, next) => {
  const start = process.hrtime.bigint()
  await next()

  const rtime = Number((process.hrtime.bigint() - start) / ms)
  const { url, method, status } = ctx

  // в режиме дебага дополнительно выводить тело запроса и ответа
  logger.debug('REQ: %s %s %j\nRES: %d %j', 'http', method, url, ctx.request.body, status, ctx.body)

  const level = status >= 500 ? 'error' : status >= 400 ? 'warn' : 'info'
  logger[level](`[${status}] ${method} ${url} - ${rtime}ms`, 'http')
}

/**
 * Обработчик всех ошибок возникающих при обработке запроса
 */
export const errorHandler: Middleware = async (ctx, next) => {
  try {
    await next()
  } catch (error: any) {
    ctx.status = error.status || 500
    ctx.body = ctx.status === 500 ? 'Internal error' : error.message

    const level = ctx.status >= 500 ? 'error' : 'warn'
    logger[level](`${ctx.method} ${ctx.url}: ${error.message}`, 'http')

    if (level === 'error') logger.error(error, 'http')
  }
}

export const cors: Middleware = async (ctx, next) => {
  const origin = ctx.get('Origin')
  const headers = ctx.get('Access-Control-Request-Headers')

  if (!origin) return next()

  ctx.set({
    'Access-Control-Allow-Origin': origin,
    'Access-Control-Allow-Methods': 'GET,HEAD,POST,PATCH,PUT,DELETE',
    'Access-Control-Allow-Headers': headers,
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Max-Age': '600',
  })

  if (ctx.method === 'OPTIONS') {
    ctx.status = 200
  } else {
    return next()
  }
}
