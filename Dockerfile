FROM node:16.10-alpine

COPY . /code

WORKDIR /code

RUN set -ex && \
  npm ci && \
  npm run build && \
  # generate API documentation
  npm run apidoc && \
  # remove devDependencies from node_modules
  npm prune --production && \
  # remove unnecessary now source files
  rm -rf /code/src && \
  node -v

ENV NODE_ENV=production
ENTRYPOINT [ "node", "dist/app.js" ]

ARG BUILD_INFO="no info"
ENV BUILD_INFO=$BUILD_INFO
