import Http from '../src/shared/http'

const client = new Http('http://127.0.0.1:3000')

test('Promos', async () => {
  let r = await client.xpost('/promo', {
    name: '1ed',
    description: 'dewdew',
  })
  console.log(r)

  r = await client.xpost('/promo/1/raffle')
  console.log(r)
})
