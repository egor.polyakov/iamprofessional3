/**
 * Создать файл с шаблоном миграции в папке src/migrations
 * Имя файла должно быть указано аргументом коммандной строки при запуске скрипта
 */

const fs = require('fs')

const red = (text) => '\u001b[31m' + text + '\u001b[0m'
const green = (text) => '\u001b[32m' + text + '\u001b[0m'

const name = process.argv[2]

if (!name) {
  console.error(red('Не указано имя миграции\n'))
  process.exit()
}

const ts = Date.now()
const file = `${ts}-${name}.ts`
const path = './src/migrations/' + file

const template = `import Connection from '../db/db.connection'

module.exports.up = async (db: Connection) => {}

module.exports.down = async (db: Connection) => {}
`

fs.writeFileSync(path, template)

console.log(green(`Created migration ${path}`))
